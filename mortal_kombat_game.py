"""
Implementation of the Mortal Kombat game between two Fighters.
Author: Ondřej Bazala
"""

import random
import time


class FighterCollection:
    """
    Contains many fighters.
    """
    def __init__(self):
        "Initializes fighters."
        attacks = AttackCollection()
        self.zeman = Fighter('Miloš Zeman',
                             95, 100, 80, attacks.zeman_attacks)
        self.tomio = Fighter('Tomio Okamura',
                             80, 210, 180, attacks.tomio_attacks)
        self.rozner = Fighter('Miloslav Rozner',
                              20, 170, 140, attacks.rozner_attacks)
        self.babis = Fighter('Andrej Babiš',
                             80, 205, 130, attacks.babis_attacks)
        self.jolanda = Fighter("Jolanda",
                               70, 140, 120, attacks.jolanda_attacks)
        self.blanik = Fighter("Tonda Blaník",
                              85, 175, 200, attacks.blanik_attacks)
        self.feri = Fighter("Dominik Feri",
                            40, 220, 195, attacks.feri_attacks)
        self.klaus_mladsi = Fighter("Václav Klaus ml.",
                                    70, 165, 140, attacks.klaus_mladsi_attacks)
        self.tvoje_mama = Fighter("Tvoja mama",
                                  100, 175, 170, attacks.tvoje_mama_attacks)
        self.robejsek = Fighter("Petr Robejšek",
                                55, 130, 140, attacks.robejsek_attacks)
        self.trump = Fighter("Donald Trump",
                             75, 180, 170, attacks.trump_attacks)
        self.internet = Fighter("Internet",
                                90, 180, 130, attacks.internet_attacks)

    def get_list(self):
        "Returns a list of fighters."
        return [self.zeman, self.tomio, self.rozner, self.babis, self.jolanda,
                self.blanik, self.feri, self.klaus_mladsi, self.tvoje_mama,
                self.robejsek, self.trump, self.internet]


class AttackCollection:
    """
    Contains many attacks.
    """
    def __init__(self):
        "Initializes attacks."
        self.zeman_attacks = [Attack('Mluvit anglicky',
                                     60, 0.85, 40),
                              Attack('Ukázat holí',
                                     130, 0.5, 70),
                              Attack('Udělat aligátora',
                                     30, 0.75, 15)]

        self.tomio_attacks = [Attack('Hodit ban',
                                     25, 0.85, 15),
                              Attack('Hodit činku',
                                     75, 0.6, 70),
                              Attack('Hodit uprchlíka',
                                    110, 0.3, 100)]

        self.rozner_attacks = [Attack('Teď jsem úplně neposlouchal.',
                                      15, 0.7, 5),
                               Attack('Odbyrokritizovat',
                                      25, 0.9, 20),
                               Attack('Asi mě váš program nezajímal.',
                                      65, 0.85, 75)]

        self.babis_attacks = [Attack('Je to kampaň vedená Kalouskem!',
                                     40, 0.9, 40),
                              Attack('Nabídnout koblihu',
                                     15, 0.8, 15),
                              Attack('Naši na ně kleknou.',
                                     100, 0.65, 80)]

        self.jolanda_attacks = [Attack('Jááj, hoď tam tu sůl!',
                                       15, 0.8, 10),
                                Attack('Velký špatný.',
                                       60, 0.65, 45),
                                Attack('Odejdeš, tam, kde žiješ.',
                                       95, 0.55, 75)]

        self.blanik_attacks = [Attack('Kafe!',
                                      20, 0.8, 5),
                               Attack('Jet jako parník',
                                      75, 0.85, 100),
                               Attack('Nechat to na Žížalovi',
                                      30, 0.4, 15)]

        self.feri_attacks = [Attack('Sdílet Meme',
                                    75, 0.45, 60),
                             Attack('Zakrýt výhled afrem',
                                    30, 0.7, 30),
                             Attack('Být černoch',
                                    50, 0.8, 50)]

        self.klaus_mladsi_attacks = [Attack('Dělat blbé ksichty',
                                            35, 0.95, 40),
                                     Attack('Vydat nový \'KOMENTÁŘ\'',
                                            80, 0.75, 70),
                                     Attack('Tatínek vám vyhlásí válku!',
                                            10, 0.6, 5)]

        self.tvoje_mama_attacks = [Attack('Pusa na dobrou noc',
                                          90, 0.7, 80),
                                   Attack('Vyvětrej si tady.',
                                          45, 0.5, 20),
                                   Attack('Zuby sis vyčistil?',
                                          25, 0.6, 20)]

        self.robejsek_attacks = [Attack('Pro mámu.',
                                        55, 0.75, 50),
                                 Attack('Pro tátu.',
                                        70, 0.4, 30),
                                 Attack('Pro děti.',
                                        10, 0.95, 5)]

        self.trump_attacks = [Attack('Stisknout červené tlačítko',
                                     100, 0.25, 40),
                              Attack('Neučesat se',
                                     30, 0.80, 30),
                              Attack('Udělat Ameriku great znova',
                                     50, 0.4, 10)]

        self.internet_attacks = [Attack('Špatně zadané WiFi heslo.',
                                        35, 0.6, 25),
                                 Attack('Vyčerpali jste svá data.',
                                        75, 0.8, 70),
                                 Attack('Stránka nenalezena.',
                                        20, 0.95, 10)]


class ComputerStrategy:
    """
    Computers AI for singleplayer.
    """
    def __init__(self, fighter, enemy):
        "Initializes fighter and enemy."
        self.fighter = fighter
        self.enemy = enemy

    def choose_move(self):
        "Chooses the move of the computer."
        attacks_new = self.order_attacks()
        enemy = self.enemy
        fighter = self.fighter
        best_attack = self.lowest_energy_attack()

        for num, attack in enumerate(attacks_new):
            if attack.energy_cost <= fighter.energy:
                if num != 2 and attack.chance_to_hit < attacks_new[num + 1]\
                   .chance_to_hit and attacks_new[num + 1].average_power >\
                   enemy.health and attacks_new[num + 1].energy_cost <= fighter.energy:
                    continue
                if attack.average_power >= enemy.health + 5 and\
                   attack.chance_to_hit > 0.3:
                    return num
                elif attack.average_power * 2 >= enemy.health + 10 and\
                     attack.energy_cost * 2 <= fighter.energy\
                     + int(fighter.max_energy / 10):
                    return num
                elif attacks_new[best_attack].average_power <\
                     attack.average_power and attack.chance_to_hit > 0.2:
                    best_attack = num
                if fighter.health < int(fighter.max_health / 10 * 2)\
                   or fighter.energy < int(fighter.max_energy / 10):
                    return 3
                if num == 2 and attacks_new[best_attack].energy_cost <=\
                   fighter.energy:
                    if attacks_new[best_attack].average_power <= 20:
                        return 3
                    else:
                        return best_attack
        return 3  # rest

    def lowest_energy_attack(self):
        "Returns the weakest attack."
        output = 0
        attacks_new = self.order_attacks()
        for i in range(len(self.fighter.attacks)):
            if attacks_new[i].energy_cost < \
               attacks_new[output].energy_cost:
                output = i
        return output

    def order_attacks(self):
        attacks = self.fighter.attacks
        return sorted(attacks, key=lambda attack: attack.energy_cost,
                   reverse=True)

    def final_choose(self):
        sorted_num = self.choose_move()
        attacks_new = self.order_attacks()
        if sorted_num == 3:
            return 3
        for i in range(3):
            if attacks_new[sorted_num].name == self.fighter.attacks[i].name:
                return i


class Attack:
    """
    Represents one type of an attack.
    """
    def __init__(self, name, average_power, chance_to_hit, energy_cost):
        "Initialize attributes."
        self.name = name
        self.average_power = average_power
        self.chance_to_hit = chance_to_hit
        self.energy_cost = energy_cost

    def __str__(self):
        "Information about object if printed."
        string = "{} is attack of average power {} chance to hit {} and" +\
               "energy cost {}"
        return string.format(self.name, self.average_power,
                             self.chance_to_hit, self.energy_cost)

    def get_power(self, level):
        "Returns random damage around average power of an attack."
        min_power = int(((self.average_power / 10) * 7) +
                        (self.average_power * (level / 500)))
        max_power = int(self.average_power / 10 * 13)
        return random.randint(min_power, max_power)


class Fighter:
    """
    Represents a championship fighter (human or anything else).
    """
    def __init__(self, name, level, max_health, max_energy, attacks):
        "Initialize attributes."
        self.name = name
        self.level = level
        self.health = max_health
        self.max_health = max_health
        self.energy = max_energy
        self.max_energy = max_energy
        self.attacks = attacks

    def __str__(self):
        "Information about object if printed."
        string = "{} is level {} with health {}/{} energy {}/{}." +\
                 "His attacks are {}, {} and {}."
        return string.format(self.name, self.level, self.health,
                             self.max_health, self.energy,
                             self.max_energy, self.attacks[0].name,
                             self.attacks[1].name, self.attacks[2].name)

    def regenerate(self, defender):
        "Gives a defender new energy each round."
        self.energy += int(self.max_energy / 10)
        if self.energy > self.max_energy:
            self.energy = self.max_energy
        print(">", defender.name, "got", int(self.max_energy / 10),
              "energy\n\n")

    def is_defeated(self):
        "Checks if both fighters are alive."
        return self.health <= 0

    def surrender(self):
        "Attacker surrenders."
        print("\n>", self.name, "surrendered.\n")
        self.health = 0

    def rest(self):
        "Attacker gets health and energy instead of attacking."
        energy = int(self.max_energy / 10 * 1.5)
        health = int(self.max_health / 10 * 1.5)
        self.energy += energy
        self.health += health
        if self.energy > self.max_energy:
            self.energy = self.max_energy
        if self.health > self.max_health:
            self.health = self.max_health
        print("\n> {} rested and gained {} health and {} energy."
              .format(self.name, health, energy))

    def perform_attack(self, attack_num):
        "Attacker lose energy for an attack."
        attack = self.attacks[attack_num]
        self.energy -= attack.energy_cost
        print("\n>", self.name, "used '" + attack.name + "'")

    def take_damage(self, attack_num, attacker):
        "Defender takes a damage or evades an attack."
        attack = attacker.attacks[attack_num]
        if attack.chance_to_hit >= random.random():
            damage = attack.get_power(attacker.level)
            self.health -= damage
            if self.health <= 0:
                print(">", self.name, "died")
            else:
                print(">", self.name, "was hit and lost",
                      damage, "health")
        else:
            print(">", self.name, "evaded the attack")


class Match:
    """
    Represents a match between two Fighters.
    """
    def __init__(self):
        "Initialize attributes."
        fighters = FighterCollection()
        self.fighters = fighters.get_list()
        self.attacker = None
        self.defender = None

    def begin(self):
        "Main method to start the game."
        multiplayer, i_starts = self.settings()
        self.game(multiplayer, i_starts)

    def settings(self):
        "Set the settings according to the player choices."
        print("-" * 78)
        print("[1] singleplayer")
        print("[2] multiplayer")
        print("-" * 78)
        game_mode = self.get_game_mode()
        print("-" * 78)
        print("     {:<30} {:<10} {:<10} {}"
              .format("name", "health", "energy", "level"))
        for i in range(len(self.fighters)):
            time.sleep(.1)
            print("{:<4} {:<30} {:<10} {:<10} {}"
                  .format("[" + str(i + 1) + "]", self.fighters[i].name,
                          self.fighters[i].health,
                          self.fighters[i].energy, self.fighters[i].level))
        print("-" * 78)
        player1, player2 = self.print_set_attacker_defender(game_mode)
        i_start = self.set_attacker_defender(player1, player2)
        return game_mode == 2, i_start

    def get_game_mode(self):
        "Recursive function which returns desired gamemode of the player."
        game_mode = input("Select game mode: ")
        if game_mode not in ["1", "2"]:
            return self.get_game_mode()
        else:
            return int(game_mode)

    def print_set_attacker_defender(self, game_mode):
        "Returns the fighters choosed from the menu."
        if game_mode == 2:  # multiplayer
            player1 = self.get_fighter("Player 1")
            player2 = self.get_fighter("Player 2", player1 + 1)
            print("\n> Player 1 chose '" + self.fighters[player1].name + "'")
            time.sleep(1)
            print("> Player 2 chose '" + self.fighters[player2].name + "'")
        else:  # singleplayer
            player1 = self.get_fighter()
            player2 = self.get_fighter("Computer", player1 + 1)
            print("\n> You chose '" + self.fighters[player1].name + "'")
            time.sleep(1)
            print("> Computer chose '" + self.fighters[player2].name + "'")
            time.sleep(1)
        return player1, player2

    def get_fighter(self, player="Player", chosen=0):
        "Returns the number of the choosed fighter."
        while True:
            try:
                if player != "Computer":
                    fighter = int(input(str(player) + " chooses a fighter: "))
                else:
                    fighter = random.randint(1, len(self.fighters))
            except ValueError:
                continue
            if fighter not in range(1, len(self.fighters) + 1):
                continue
            if fighter == chosen:
                if player != "Computer":
                    print("Choose different fighter than your enemy.")
                continue
            return fighter - 1

    def set_attacker_defender(self, player1, player2):
        "Decides who from the fighters will be starting the round."
        if self.fighters[player1].level < self.fighters[player2].level:
            self.attacker = self.fighters[player1]
            self.defender = self.fighters[player2]
            return 0
        elif self.fighters[player1].level > self.fighters[player2].level:
            self.attacker = self.fighters[player2]
            self.defender = self.fighters[player1]
            return 1
        elif self.fighters[player1].energy > self.fighters[player2].energy:
            self.attacker = self.fighters[player1]
            self.defender = self.fighters[player2]
            return 0
        else:
            self.attacker = self.fighters[player2]
            self.defender = self.fighters[player1]
            return 1

    def game(self, multiplayer, i_starts):
        "Controls the game."
        print("> The match begins!\n")
        if i_starts == 1:
            computer = ComputerStrategy(self.attacker, self.defender)
        else:
            computer = ComputerStrategy(self.defender, self.attacker)
        i = i_starts
        while not self.attacker.is_defeated()\
              and not self.defender.is_defeated():
            if i % 2 == 0 or multiplayer:
                winner = self.perform_round(True)
            else:
                winner = self.perform_round(False, computer)
            i += 1
            self.change_attacker()
        print("> " + winner, "won\n")
        self.restart()

    def perform_round(self, multiplayer, computer=None):
        "Controls a round of the match."
        time.sleep(1)
        self.print_state()
        time.sleep(1)
        self.print_moves()
        if multiplayer:
            attack_num = self.choose_move()
        else:
            attack_num = computer.final_choose()
            print("Computer choosed the move:", attack_num + 1)
        time.sleep(1)
        if attack_num + 1 == 4:
            self.attacker.rest()
        elif attack_num + 1 == 5:
            self.attacker.surrender()
            return self.defender.name
        else:
            self.attacker.perform_attack(attack_num)
            time.sleep(1.5)
            self.defender.take_damage(attack_num, self.attacker)
            time.sleep(1.5)
        if not self.defender.is_defeated():
            self.defender.regenerate(self.defender)
        else:
            return self.attacker.name

    def print_state(self):
        "Prints health and energy of both fighters."
        attacker = self.attacker
        defender = self.defender
        print("\n{:<25}{}\t\t{}".format("Name", "Health", "Energy"))
        print("{:<25}{} / {}\t{} / {}\n"
              .format(attacker.name, attacker.health, attacker.max_health,
                      attacker.energy, attacker.max_energy))
        print("{:<25}{} / {}\t{} / {}\n"
              .format(defender.name, defender.health, defender.max_health,
                      defender.energy, defender.max_energy))

    def print_moves(self):
        "Prints all possible moves of the attacker."
        attacks = self.attacker.attacks
        print("+{:-^77}+".format(" " + self.attacker.name + " "))
        print("     {:<30} {:<15} {:<15} {}"
              .format("Name", "Average power", "Chance to hit", "Energy cost"))
        for i in range(3):
            print(str(i+1), "-- {:<30} {:<15} {:<15} {}"
                  .format(attacks[i].name, attacks[i].average_power,
                          attacks[i].chance_to_hit, attacks[i].energy_cost))
        print("+" + "-" * 77 + "+")
        energy = int(self.attacker.max_energy / 10 * 1.5)
        health = int(self.attacker.max_health / 10 * 1.5)
        print("4 -- Rest" + " " * 27 + "gain", health, "health and", energy,
              "energy.")
        print("5 -- Surrender" + " " * 22 + "be defeated")
        print("+" + "-" * 77 + "+")

    def choose_move(self):
        "Gets number of attack from the player."
        while True:
            try:
                attack_num = int(input("Choose the move: "))
            except ValueError:
                continue
            attack_num -= 1
            if attack_num < 0 or attack_num > 4:
                continue
            if attack_num < 3 and self.attacker.attacks[attack_num]\
               .energy_cost > self.attacker.energy:
                print("Not enough energy.")
                continue
            
            return attack_num

    def change_attacker(self):
        "Changes the role of the fighters."
        tmp_attacker = self.attacker
        self.attacker = self.defender
        self.defender = tmp_attacker

    def restart(self):
        "Can restart the game."
        while True:
            restart = input("Play again? (y/n) ")
            if restart not in ["y", "n"]:
                continue
            if restart == "y":
                fighters = FighterCollection()
                self.fighters = fighters.get_list()
                self.attacker = None
                self.defender = None
                print("\n\n\n")
                self.begin()
                break
            else:
                restart = ""
                break


# === Demo ===

def play():
    "main function which runs the program."
    match = Match()
    match.begin()


if __name__ == '__main__':
    play()
